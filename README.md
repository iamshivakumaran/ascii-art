# ASCII ART FOR RETROS🎭
- This app is designed for basics of creating an Colored ASCII.
- Just for enjoyment purpose.
---
## Prequsities
- Python above 3.x
- Termcolor
---
## File Preview
1. _File can be found at [here](code/ascii_art.py)_
---
## Python with its modules
```bash
pip3 install termcolor
```
---
## Linux by using `Nala` under root permissions(Recommends for a change from `apt` to `nala`)

### It can be skipped

Nala can be installed from Volian Scar repository in Debian, Ubuntu and its variants.

1. Update the sources list with `Latest` version of Debian based systems
```bash
$ sudo apt update && sudo apt install nala
```
2. Update the sources list with `LTS` versions of Debian based systems.
```bash
$ sudo apt update && sudo apt install nala-legacy
```
---
## About Me
- Hardware 🐾
-  Software 😁
