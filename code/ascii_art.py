import termcolor as tmp

emi = input("Enter your name: ")
#Corrections are adopted
for ling1 in emi:
    ling1 = ling1.upper()
    if (ling1 in "A"):
        tmp.cprint("..######..\n..#....#..\n..######..\n..#....#..\n..#....#..\n\n","red")
    elif (ling1 in "B"):
        tmp.cprint("..######..\n..#....#..\n..#####...\n..#....#..\n..######..\n\n","red")
    elif (ling1 in "C"):
        tmp.cprint("..######..\n..#.......\n..#.......\n..#.......\n..######..\n\n","red")
    elif (ling1 in "D"):
        tmp.cprint("..#####...\n..#....#..\n..#....#..\n..#....#..\n..#####...\n\n","red")
    elif (ling1 in "E"):
        tmp.cprint("..######..\n..#.......\n..#####...\n..#.......\n..######..\n\n","green")
    elif (ling1 in "F"):
        tmp.cprint("..######..\n..#.......\n..#####...\n..#.......\n..#.......\n\n","green")
    elif (ling1 in "G"):
        tmp.cprint("..######..\n..#.......\n..#####...\n..#....#..\n..#####...\n\n","green")
    elif (ling1 in "H"):
        tmp.cprint("..#....#..\n..#....#..\n..######..\n..#....#..\n..#....#..\n\n","green")
    elif (ling1 in "I"):
        tmp.cprint("..######..\n....##....\n....##....\n....##....\n..######..\n\n","blue")
    elif (ling1 in "J"):
        tmp.cprint("..######..\n....##....\n....##....\n..#.##....\n..####....\n\n","blue")
    elif (ling1 in "K"):
        tmp.cprint("..#...#...\n..#..#....\n..##......\n..#..#....\n..#...#...\n\n","blue")
    elif (ling1 in "L"):
        tmp.cprint("..#.......\n..#.......\n..#.......\n..#.......\n..######..\n\n","blue")
    elif (ling1 in "M"):
        tmp.cprint("..#....#..\n..##..##..\n..#.##.#..\n..#....#..\n..#....#..\n\n","yellow")
    elif (ling1 in "N"):
        tmp.cprint("..#....#..\n..##...#..\n..#.#..#..\n..#..#.#..\n..#...##..\n\n","yellow")
    elif (ling1 in "O"):
        tmp.cprint("..######..\n..#....#..\n..#....#..\n..#....#..\n..######..\n\n","yellow")
    elif (ling1 in "P"):
        tmp.cprint("..######..\n..#....#..\n..######..\n..#.......\n..#.......\n\n","yellow")
    elif (ling1 in "Q"):
        tmp.cprint("..######..\n..#....#..\n..#.#..#..\n..#..#.#..\n..######..\n\n","magenta")
    elif (ling1 in "R"):
        tmp.cprint("..######..\n..#....#..\n..#.##...\n..#...#...\n..#....#..\n\n","magenta")
    elif (ling1 in "S"):
        tmp.cprint("..######..\n..#.......\n..######..\n.......#..\n..######..\n\n","magenta")
    elif (ling1 in "T"):
        tmp.cprint("..######..\n....##....\n....##....\n....##....\n....##....\n\n","magenta")
    elif (ling1 in "U"):
        tmp.cprint("..#....#..\n..#....#..\n..#....#..\n..#....#..\n..######..\n\n","cyan")
    elif (ling1 in "V"):
        tmp.cprint("..#....#..\n..#....#..\n..#....#..\n...#..#...\n....##....\n\n","cyan")
    elif (ling1 in "W"):
        tmp.cprint("..#....#..\n..#....#..\n..#.##.#..\n..##..##..\n..#....#..\n\n","cyan")
    elif (ling1 in "X"):
        tmp.cprint("..#....#..\n...#..#...\n....##....\n...#..#...\n..#....#..\n\n","cyan")
    elif (ling1 in "Y"):
        tmp.cprint("..#....#..\n...#..#...\n....##....\n....##....\n....##....\n\n",None,"on_blue")
    elif (ling1 in "Z"):
        print("..######..\n......#...\n.....#....\n....#.....\n..######..\n\n",None,"on_blue")
    elif (ling1 == " "):
        print("..........\n..........\n..........\n..........\n\n")
    elif (ling1 in "."):
        tmp.cprint("----..----\n\n","cyan",attrs=["bold"])